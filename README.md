# Advance Script Manager

Advance script manager provides functionality to add and manage JavaScript and
CSS to the website.
It allows placing JS and CSS on pages similer to blocks.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/advance_script_manager).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/advance_script_manager).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module don't require any other module then drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Configure the admin toolbar tools at (/admin/config/development/advance-script-manager).

## Maintainers

Current maintainers:

- [Subhransu Sekhar](https://www.drupal.org/u/subhransu-sekhar)
- [Kapil Varshney (KapilV)](https://www.drupal.org/u/kapilv)

Supporting organizations:

- [Innoraft](https://www.drupal.org/innoraft) Created this module for you!
