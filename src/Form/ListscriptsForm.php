<?php

namespace Drupal\advance_script_manager\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ListscriptsForm for listing scripts.
 */
class ListscriptsForm extends FormBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Header variable.
   *
   * @var array
   */

  protected $header;

  /**
   * The current request on url.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * A plugin cache clear instance.
   *
   * @var \Drupal\Core\Plugin\CachedDiscoveryClearerInterface
   */
  protected $pluginCacheClearer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->messenger = $container->get('messenger');
    $instance->database = $container->get('database');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->requestStack = $container->get('request_stack');
    $instance->pluginCacheClearer = $container->get('plugin.cache_clearer');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'listscripts_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->header = [
      'script_name' => $this->t('Script name'),
      'visibility_section' => $this->t('Visibility section'),
      'created' => $this->t('Created'),
      'updated' => $this->t('Updated'),
      'status' => $this->t('Status'),
      'action' => $this->t('Action'),
    ];
    // Get parameter value while submitting filter form.
    $visibility = $this->requestStack->getCurrentRequest()->query->get('visibility');
    $status = $this->requestStack->getCurrentRequest()->query->get('status');
    $tblrow = $this->getRecords($visibility, $status);

    $form['list_scripts'] = [
      '#type' => 'tableselect',
      '#header' => $this->header,
      '#options' => $tblrow,
      '#weight' => '0',
      '#empty' => $this->t('No records found'),
      '#attributes' => ['class' => ['advance-scripts-table']],
    ];
    $form['pager'] = [
      '#type' => 'pager',
      '#prefix' => '<div class="adv-pagination">',
      '#suffix' => '</div>',
    ];

    $form['bulk_action_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Action'),
      '#options' => [
        '0' => $this->t('-Select-'),
        'activate' => $this->t('Activate selected scripts'),
        'disable' => $this->t('Disable selected scripts'),
        'move_script_to_header' => $this->t('Move scripts to header'),
        'move_script_to_body' => $this->t('Move scripts to body'),
        'move_script_to_footer' => $this->t('Move scripts to footer'),
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply to selected scripts'),
      '#submit' => ['::submitForm'],
      '#button_type' => 'primary',
    ];

    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
      '#submit' => ['::deleteMultiple'],
      '#button_type' => 'danger',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getRecords($visibility = NULL, $status = NULL) {
    $tblrow = [];
    $query = $this->database->select('advance_script_manager', 'a');
    $query->fields('a', [
      'id',
      'script_name',
      'visibility_section',
      'created',
      'updated',
      'status',
    ]);
    if (!empty($visibility) && $visibility != '') {
      $query->condition('visibility_section', $visibility);
    }
    if (!empty($status) && $status != '') {
      $query->condition('status', $status);
    }
    $query->orderBy('weight', 'ASC');
    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(20);
    $result = $pager->execute()->fetchAll();

    foreach ($result as $row) {
      $tblrow[$row->id] = [
        'script_name' => $row->script_name,
        'visibility_section' => $row->visibility_section,
        'created' => ($row->created ? date('m/d/Y H:i:s', $row->created) : 'NULL'),
        'updated' => ($row->updated ? date('m/d/Y H:i:s', $row->updated) : 'NULL'),
        'status' => ($row->status == 1 ? $this->t('Active') : $this->t('Disabled')),
        'action' => Link::createFromRoute('Edit script', 'advance_script_manager.scripts_form', ['num' => $row->id]),
      ];
    }

    return $tblrow;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    $item_ids = $form_state->getValue('list_scripts');
    if (empty($item_ids)) {
      $this->messenger->addError($this->t('No item selected'));
      return 0;
    }

    $action = $form_state->getValue('bulk_action_type');
    switch ($action) {
      case 'disable':
        $res = $this->database->update('advance_script_manager')
          ->fields([
            'status' => 2,
            'updated' => time(),
          ])
          ->condition('id', $item_ids, 'IN')
          ->execute();
        if ($res) {
          $this->messenger->addMessage($this->t('Scripts has been deactivated.'));
        }
        break;

      case 'activate':
        $res = $this->database->update('advance_script_manager')
          ->fields([
            'status' => 1,
            'updated' => time(),
          ])
          ->condition('id', $item_ids, 'IN')
          ->execute();
        if ($res) {
          $this->messenger->addMessage($this->t('Scripts has been activated.'));
        }
        break;

      case 'move_script_to_header':
        $res = $this->database->update('advance_script_manager')
          ->fields([
            'visibility_section' => 'Header',
            'updated' => time(),
          ])
          ->condition('id', $item_ids, 'IN')
          ->execute();
        if ($res) {
          $this->messenger->addMessage($this->t('Scripts has been moved to header.'));
        }
        break;

      case 'move_script_to_body':
        $res = $this->database->update('advance_script_manager')
          ->fields([
            'visibility_section' => 'Body',
            'updated' => time(),
          ])
          ->condition('id', $item_ids, 'IN')
          ->execute();
        if ($res) {
          $this->messenger->addMessage($this->t('Scripts has been moved to body.'));
        }
        break;

      case 'move_script_to_footer':
        $res = $this->database->update('advance_script_manager')
          ->fields([
            'visibility_section' => 'Footer',
            'updated' => time(),
          ])
          ->condition('id', $item_ids, 'IN')
          ->execute();
        if ($res) {
          $this->messenger->addMessage($this->t('Scripts has been moved to footer.'));
        }
        break;

      default:
        // code...
        break;
    }
    $this->pluginCacheClearer->clearCachedDefinitions();

  }

  /**
   * {@inheritdoc}
   */
  public function deleteMultiple(array &$form, FormStateInterface $form_state) {
    $item_ids = $form_state->getValue('list_scripts');
    foreach ($item_ids as $key => $value) {
      if ($value == 0) {
        unset($item_ids[$key]);
      }
    }
    $ids = implode("|", $item_ids);
    if (!empty($ids)) {
      $url = Url::fromRoute('advance_script_manager.delete_multiple_form', ['ids' => $ids]);
      $form_state->setRedirectUrl($url);
    }
    else {
      $this->messenger->addError($this->t('No items selected'));
    }
  }

}
