<?php

namespace Drupal\advance_script_manager\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Delete Lists.
 */
class ListScriptsFormDeleteMultiple extends ConfirmFormBase {

  /**
   * Database connection object.
   *
   * @var Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructor.
   *
   * @param Drupal\Core\Database\Connection $database
   *   Database connection object.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'advance_script_manager_delete_multiple_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete below listed scripts?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $html = 'This action cannot be undon.';
    $html .= $this->getItemlist();
    return $html;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('advance_script_manager.advance_script_controller_build');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $ids = $this->requestStack->getCurrentRequest()->get('ids');
    $id = explode('|', $ids);
    if (!empty($id)) {
      $res = $this->database->delete('advance_script_manager')
        ->condition('id', $id, 'IN')
        ->execute();
      if ($res) {
        \Drupal::service('plugin.cache_clearer')->clearCachedDefinitions();
        $this->messenger()->addMessage($this->t('Script has been deleted.'));
        $form_state->setRedirect('advance_script_manager.advance_script_controller_build');
      }
    }
    else {
      $this->messenger()->addError($this->t('Script was not deleted, Something went wronge.'));
      $form_state->setRedirect('advance_script_manager.advance_script_controller_build');
    }
  }

  /**
   * Function to get CurrentItemsList.
   */
  public function getItemlist() {
    $ids = \Drupal::request()->get('ids');
    $id = explode('|', $ids);
    $query = $this->database->select('advance_script_manager', 'asm');
    $query->fields('asm', ['script_name']);
    $query->condition('asm.id', $id, 'IN');
    $res = $query->execute()->fetchAll();
    $html = '<ul>';
    foreach ($res as $value) {
      $html .= '<li>' . $value->script_name . '</li>';
    }
    $html .= '</ul>';
    return $html;
  }

}
