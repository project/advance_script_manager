<?php

namespace Drupal\advance_script_manager\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * DeleteForm.
 */
class ScriptsFormDelete extends ConfirmFormBase {

  /**
   * Database connection object.
   *
   * @var Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'advance_script_manager_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this script?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('advance_script_manager.advance_script_controller_build');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $id = $this->requestStack->getCurrentRequest()->get('id');
    if (!empty($id)) {
      $res = $this->database->delete('advance_script_manager')
        ->condition('id', $id, '=')
        ->execute();
      if ($res) {
        \Drupal::service('plugin.cache_clearer')->clearCachedDefinitions();
        $this->messenger()->addMessage($this->t('Script has been deleted.'));
        $form_state->setRedirect('advance_script_manager.advance_script_controller_build');
      }
    }
    else {
      $this->messenger()->addError($this->t('Script was not deleted, Something went wronge.'));
      $form_state->setRedirect('advance_script_manager.advance_script_controller_build');
    }
  }

}
