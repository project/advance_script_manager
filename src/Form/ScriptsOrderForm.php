<?php

namespace Drupal\advance_script_manager\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Script Order Form.
 *
 * @ingroup Advance Script Manager
 */
class ScriptsOrderForm extends FormBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * A plugin cache clear instance.
   *
   * @var \Drupal\Core\Plugin\CachedDiscoveryClearerInterface
   */
  protected $pluginCacheClearer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('plugin.cache_clearer'),
    );
  }

  /**
   * Construct a form.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $pluginCacheClearer
   *   A plugin cache clear instance.
   */
  public function __construct(Connection $database, CachedDiscoveryClearerInterface $pluginCacheClearer) {
    $this->database = $database;
    $this->pluginCacheClearer = $pluginCacheClearer;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'script_order_form';
  }

  /**
   * Builds the Script Order Form.
   *
   * @param array $form
   *   Render array representing from.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['back'] = [
      '#type' => 'submit',
      '#value'  => $this->t('Back'),
      '#submit' => ['::cancel'],
    ];
    $form['table-row'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Script name'),
        $this->t('Visibility Section'),
        $this->t('Status'),
        $this->t('Weight'),
      ],
      '#empty' => $this->t('Sorry, There are no items!'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'asm-table-sort-weight',
        ],
      ],
    ];

    $results = $this->database->select('advance_script_manager', 't')
      ->fields('t')
      ->orderBy('weight')
      ->execute()
      ->fetchAll();
    $weightRange = count($results);
    foreach ($results as $row) {
      $form['table-row'][$row->id]['#attributes']['class'][] = 'draggable';
      $form['table-row'][$row->id]['#weight'] = $row->weight;
      $form['table-row'][$row->id]['script_name'] = [
        '#markup' => $row->script_name,
      ];
      $form['table-row'][$row->id]['visibility_section'] = [
        '#markup' => $row->visibility_section,
      ];
      $form['table-row'][$row->id]['status'] = [
        '#markup' => $row->status == 1 ? $this->t('Active') : $this->t('Disabled'),
      ];
      $form['table-row'][$row->id]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $row->script_name]),
        '#title_display' => 'invisible',
        '#delta' => $weightRange,
        '#default_value' => $row->weight,
        '#attributes' => ['class' => ['asm-table-sort-weight']],
      ];
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'submit',
      '#value'  => $this->t('Cancel'),
      '#submit' => ['::cancel'],
    ];

    return $form;
  }

  /**
   * Form submission handler for the 'Return to' action.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function cancel(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('advance_script_manager.advance_script_controller_build');
  }

  /**
   * Form submission handler for the Script Order Form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $submission = $form_state->getValue('table-row');
    foreach ($submission as $id => $item) {
      $this->database->update('advance_script_manager')
        ->fields([
          'weight' => $item['weight'],
        ])
        ->condition('id', $id, '=')
        ->execute();
    }
    $this->pluginCacheClearer->clearCachedDefinitions();
    $this->messenger()->addMessage($this->t('Order saved'));
  }

}
