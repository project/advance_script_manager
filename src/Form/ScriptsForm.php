<?php

namespace Drupal\advance_script_manager\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ScriptsForm used for script entry form.
 */
class ScriptsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entitytypemanager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current request on url.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * A plugin cache clear instance.
   *
   * @var \Drupal\Core\Plugin\CachedDiscoveryClearerInterface
   */
  protected $pluginCacheClearer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->database = $container->get('database');
    $instance->messenger = $container->get('messenger');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->requestStack = $container->get('request_stack');
    $instance->pluginCacheClearer = $container->get('plugin.cache_clearer');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'advance_script_manager.scripts',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'scripts_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $request_id = $this->requestStack->getCurrentRequest()->query->get('num');
    $data = $this->getSpecificRecord($request_id);
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Script name'),
      '#size' => 64,
      '#required' => 'true',
      '#description' => $this->t('Enter a name to describe the code block'),
      '#default_value' => $data['script_name'] ?? '',
    ];
    $form['enable_code_script'] = [
      '#type' => 'radios',
      '#title' => $this->t('Enable code script'),
      '#options' => [
        '1' => $this->t('Active'),
        '2' => $this->t('Disabled'),
      ],
      '#description' => $this->t("Scripts code snippets are disabled by default, so you won\'t accidentally make the code live."),
      '#default_value' => $data['status'] ?? 2,
    ];
    $form['script_code'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Java script code'),
      '#description'   => $this->t('<p>You can add multiple <strong>scripts</strong> here with multiple ways, For example: </p><p>1. &lt;script type="text/javascript" src="http://www.example.com/script.js"&gt;&lt;/script&gt;</p><p> 2. &lt;script type="text/javascript" src="/script.js"&gt;&lt;/script&gt;</p><p> 3. &lt;script type="text/javascript"&gt;console.log("HFS Header");&lt;/script&gt;</p>'),
      '#rows'          => 10,
      '#default_value' => $data['script_code'] ?? '',
    ];
    $form['css_code'] = [
      '#type' => 'textarea',
      '#title' => $this->t('CSS code'),
      '#description'   => $this->t('<p>You can add multiple <strong>stylesheets</strong> here with multiple ways, For example: </p><p>1. &lt;link type="text/css" rel="stylesheet" href="http://www.example.com/style.css" media="all" /&gt;</p><p> 2. &lt;link type="text/css" rel="stylesheet" href="/style.css" media="all" /&gt;</p><p> 3. &lt;style&gt;#header { color: grey; }&lt;/style&gt;</p>'),
      '#rows'          => 10,
      '#default_value' => $data['css_code'] ?? '',
    ];

    $form['information'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-publication',
    ];
    $form['visibility'] = [
      '#type' => 'details',
      '#title' => $this
        ->t('Visibility Settings'),
      '#group' => 'information',
    ];
    $form['visibility']['visibility_settings'] = [
      '#type' => 'select',
      '#title' => $this->t('Visibility settings'),
      '#options' => [
        'Header' => $this->t('Header'),
        'Footer' => $this->t('Footer'),
        'Body' => $this->t('Body'),
      ],
      '#size' => 1,
      '#default_value' => $data['visibility_section'] ?? 'Header',
    ];
    $form['page'] = [
      '#type' => 'details',
      '#title' => $this
        ->t('Pages Settings'),
      '#group' => 'information',
    ];
    $form['page']['visibility_pages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages'),
      '#description' => $this->t('Specify the pages by using their paths. Enter one path per line. &ltfront&gt is the front page'),
      '#default_value' => $data['visibility_pages'] ?? '',
    ];
    $form['page']['pages_settings'] = [
      '#type' => 'radios',
      '#title' => $this->t('Invoke script code on specific pages'),
      '#options' => [
        'all' => $this->t('All pages expected those listed'),
        'only' => $this->t('Only the listed pages'),
      ],
      '#default_value' => $data['pages_settings'] ?? '',
    ];
    $form['roles'] = [
      '#type' => 'details',
      '#title' => $this
        ->t('Role Settings'),
      '#group' => 'information',
    ];
    $form['roles']['user_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('User Roles'),
      '#options' => $this->fetchAllRoles(),
      '#default_value' => isset($data['user_roles']) ? explode(',', $data['user_roles']) : [],
    ];
    $form['content'] = [
      '#type' => 'details',
      '#title' => $this
        ->t('Content Settings'),
      '#group' => 'information',
    ];
    $form['content']['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content Types'),
      '#options' => $this->fetchContentTypes(),
      '#default_value' => isset($data['content_type']) ? explode(',', $data['content_type']) : [],
    ];

    if (!empty($request_id)) {
      $form['actions']['delete'] = [
        '#type' => 'link',
        '#title' => $this->t('Delete'),
        '#url' => Url::fromRoute('advance_script_manager.delete_form', ['id' => $request_id]),
        '#attributes' => [
          'class' => ['use-ajax button button--danger'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode([
            'width' => 'auto',
          ]),
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $request_id = $this->requestStack->getCurrentRequest()->query->get('num');
    $value = $form_state->getValues();
    $content_types = implode(",", $value['content_types']);
    $user_roles = implode(",", $value['user_roles']);
    $field = [
      'script_name'   => trim($value['name']),
      'script_code' => trim($value['script_code']),
      'css_code' => trim($value['css_code']),
      'visibility_section' => trim($value['visibility_settings']),
      'pages_settings' => $value['pages_settings'],
      'visibility_pages' => trim($value['visibility_pages']),
      'content_type' => $content_types,
      'user_roles' => $user_roles,
      'status' => $value['enable_code_script'],
      'created' => time(),
      'updated' => time(),
    ];
    if (!empty($request_id)) {
      unset($field['created']);
      $field['updated'] = time();
      $res = $this->database->update('advance_script_manager')
        ->fields($field)
        ->condition('id', $request_id)
        ->execute();
      if ($res) {
        $this->messenger->addMessage($this->t('Script config updated.'));
      }
    }
    else {
      $res = $this->database->insert('advance_script_manager')
        ->fields($field)
        ->execute();
      if ($res) {
        $this->messenger->addMessage($this->t('Script config saved.'));
      }
    }
    $this->pluginCacheClearer->clearCachedDefinitions();
    $url = Url::fromRoute('advance_script_manager.advance_script_controller_build');
    $form_state->setRedirectUrl($url);
  }

  /**
   * {@inheritdoc}
   */
  public function fetchAllRoles() {
    $allroles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $data = [];
    foreach ($allroles as $value) {
      $data[$value->id()] = $value->label();
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchContentTypes() {
    $allTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $data = [];
    foreach ($allTypes as $machine_name => $value) {
      $data[$machine_name] = $value->label();
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getSpecificRecord($id) {
    $result = $this->database->select('advance_script_manager', 'a')
      ->fields('a')
      ->condition('id', $id)
      ->execute()
      ->fetchAssoc();
    return $result;
  }

}
